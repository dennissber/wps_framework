WPS Framework
==================================

WPS Framework - is a theme configuration framework for WordPress CMS.
Parent theme based on started theme for WordPress.

You can learn more about WPS Framework features from the [official documentation](https://github.com/penguin-007/wps_framework/wiki).

## Change log ##

#### v0.1.3 ####
* On element "UI_Select" add parametr "def_value"

#### v0.1.2.1 ####
* Fixed bugs

#### v0.1.2 ####
* Fixed bugs

#### v0.1.1 ####
* Fixed bugs
* Add option theme "wps_framework_options" (documentation->home)

#### v0.1.0 ####
* Start beta version